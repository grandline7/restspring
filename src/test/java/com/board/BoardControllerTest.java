package com.board;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.mockito.Matchers.isA; 
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.util.ArrayList;
import java.util.List;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import com.board.controller.BoardController;
import com.board.domain.BoardVo;
import com.board.service.BoardService;
import com.example.config.MvcConfig;
import com.example.config.RootConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={MvcConfig.class, RootConfig.class})
public class BoardControllerTest {
	
	@InjectMocks
	private BoardController boardController;
	@Mock
	BoardService boardService;
	
	@Autowired
	private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(boardController).build();
	}
	
    @Test
    public void getList() throws Exception {
    	List<BoardVo> boardList = new ArrayList<BoardVo>();
    	BoardVo board = new BoardVo();
    	board.setNum(2);
    	board.setSubject("subject");
    	board.setWriter("hello");
    	board.setContents("contents");
    	board.setHit(18);
    	board.setIp("127.0.0.1");
    	boardList.add(board);
    	when(boardService.getList()).thenReturn(boardList);
    	
        this.mockMvc.perform(get("/board/getList").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
          .andExpect(status().isOk())
          .andExpect(content().contentType("application/json;charset=UTF-8"))
          .andDo(print());
        
        verify(boardService).getList();
        verifyNoMoreInteractions(boardService);

    }
	
}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>board list</title>
<script type="text/javascript" src="<c:url value='/resources/js/jquery.min.js'/>" ></script>
<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js" charset="utf-8"></script> -->
<script type="text/javascript">

	function getList(){
		
		$.ajax({
			type: "GET",
			async: true,
			url: "<c:url value='/board/getList' />",
			success: function(data) {
				var html = "<table border='1'>";
				$.each(data, function(entryIndex, entry) {
					html += "<tr>";
					html += "<td>" + entry.num + "</td>";
					html += "<td>" + entry.subject + "</td>";
					html += "<td>" + entry.writer + "</td>";
					html += "<td>" + entry.contents + "</td>";
					html += "</tr>";
				})
				html += "</table>";
				$("#boardList").html(html);
			}
		});
	}
</script>
</head>
<body>

<input type="button" value="board list" onclick="javascript:getList();">
<div id = "boardList"></div>

</body>
</html>
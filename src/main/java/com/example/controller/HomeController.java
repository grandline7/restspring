package com.example.controller;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.slf4j.Logger;

/**
 * Created by young on 2016. 5. 23..
 */

@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
    @RequestMapping(value = "/", method = RequestMethod.GET )
    public String getDefaultIndex(Model model) {
        model.addAttribute("message", "message1");
        logger.info("HomeController.getDefaultIndex()");
        return "defaultIndex";
    }
    
    @RequestMapping(value = "/getIndex", method = RequestMethod.GET )
    public String getIndex(Model model) {
        model.addAttribute("message", "message2");
        logger.info("HomeController.getIndex()");
        return "index";
    }

}

package com.example.web;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;
import com.example.config.MvcConfig;
import com.example.config.RootConfig;

/**
 * Created by young on 2016. 5. 23..
 */
public class Initializer implements WebApplicationInitializer {
	
    @Override
    public void onStartup(ServletContext servletContext)
            throws ServletException {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(RootConfig.class);
        servletContext.addListener(new ContextLoaderListener(rootContext));

        this.addDispatcherServlet(servletContext);
        this.addUtf8CharacterEncodingFilter(servletContext);
    }

    private void addDispatcherServlet(ServletContext servletContext) {
        AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
//        applicationContext.getEnvironment().addActiveProfile("production");
        applicationContext.register(MvcConfig.class);

        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", new DispatcherServlet(applicationContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");
//        dispatcher.setInitParameter("dispatchOptionsRequest", "true");
    }

    /**
     * UTF-8 캐릭터 인코딩 필터
     * @param servletContext
     */
    private void addUtf8CharacterEncodingFilter(ServletContext servletContext) {
        FilterRegistration.Dynamic filter = servletContext.addFilter("CHARACTER_ENCODING_FILTER", CharacterEncodingFilter.class);
        filter.setInitParameter("encoding", "UTF-8");
        filter.setInitParameter("forceEncoding", "true");
        filter.addMappingForUrlPatterns(null, false, "/*");
    }
}

/*
public class Initializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	@Override  
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { RootConfig.class };  
    }   
	
	@Override  protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { MvcConfig.class };  
	}   
	
	@Override  protected String[] getServletMappings() {
		return new String[] { "/" };  
    }   
	
	@Override  protected Filter[] getServletFilters() {
		System.out.println("getServletFilters()");
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();      
		characterEncodingFilter.setEncoding("UTF-8");             
		return new Filter[] { characterEncodingFilter};     
	}
}
*/
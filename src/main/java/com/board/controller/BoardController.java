package com.board.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.board.domain.BoardVo;
import com.board.service.BoardService;

@Controller
@RequestMapping(value = "/board")
public class BoardController {
	
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
	
	@Autowired
	private BoardService boardService;
	
//	@RequestMapping(value = "/getList", method = {RequestMethod.GET, RequestMethod.POST} )
//    public String getList(Model model) {
//        logger.info("BoardController.getList()");
//        model.addAttribute("boardList", boardService.getList());
//        
//        return "/board/list";
//    }
	
	@RequestMapping(value = "/getListPage", method = {RequestMethod.GET, RequestMethod.POST} )
    public String getListPage(Model model) {
		logger.info("/getListPage");
        return "/board/list";
    }
	
    @RequestMapping(value = "/getList", method = {RequestMethod.GET, RequestMethod.POST} )
    public @ResponseBody Object getList(Model model) {
    	logger.info("/getList");
        List<BoardVo> boardList = boardService.getList();
        return boardList;
    }
}

CREATE TABLE board (
      NUM number primary key,
      SUBJECT varchar(250)  ,
      WRITER varchar(50)  ,
      CONTENTS varchar(50) ,
      HIT number(11) ,
      IP varchar(30)  ,
      REG_DATE DATE ,
      MOD_DATE DATE
    );
    
    
CREATE SEQUENCE test_sequence
START WITH 1
INCREMENT BY 1;

--트리거 걸기
CREATE OR REPLACE TRIGGER test_trigger
BEFORE INSERT
ON board
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
SELECT test_sequence.nextval INTO :NEW.NUM FROM dual;
END;                      


-- 데이터 삽입 
INSERT INTO BOARD (SUBJECT, WRITER, CONTENTS, IP, HIT)
                VALUES (132, 312, 312, 229, 0);